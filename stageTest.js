/****************************************************************************************
** stageTest.js -- Copyright Mick
** Version 1.0: 18/02/2022
*** Test du recursive en JS, avec un simple ajout de 'pa', 'pe', 'pu', 'pi', 'po'
*** après chaque voyelle correspondante dans une phrase donnée.
****************************************************************************************/
const arrayArgs = [1, 2, 3, 4];

function transform(arg) {
    // if arg is a Number
    if (typeof arg === "number") {
        switch (arg) {
            // Check what number it is, returns transform(string) depending on number
            case arrayArgs[0]: return transform("Bonjour");
            case arrayArgs[1]: return transform("Bonsoir");
            case arrayArgs[2]: return transform("BONSOIR");
            case arrayArgs[3]: return transform("abcdefghijklmnopqrstuvwxyz");
            // if looking for any other number, returns ''
            default: return '';
        }
    }
    // if arg is a String
    if (typeof arg === "string") {
        // get the string as an array
        const argArr = [...arg];
        // for each char in array...
        for (let i=0; i<argArr.length; i++) {
            const letter = argArr[i];
            // ... add 'p' or 'P' and the corresponding char (a, e, u, i, o)
            if (letter.match(/[aeuio]/)) { argArr[i] += "p" + letter; }
            else if (letter.match(/[AEUIO]/)) { argArr[i] += "P" + letter; }
        }
        // return array back to a string
        return argArr.join('');
    }
    // if arg is an Array
    if (Array.isArray(arg)) {
        // go through all elements in array to transform and join them in a string with multiple lines
        return arg.map(o => { return transform(o); }).join('\n');
    }
}

console.log(transform(arrayArgs));
